if (process.env.NODE_ENV !== "production") {
    require('dotenv').config();
}
const mongoose = require('mongoose');
const cities = require('./cities');
const { places, descriptors } = require('./seedHelpers');
const Campground = require('../models/campground');

mongoose.connect(process.env.DB_URL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
});

const db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error:"));
db.once("open", () => {
    console.log("Database connected");
});

const sample = array => array[Math.floor(Math.random() * array.length)];


const seedDB = async () => {
    await Campground.deleteMany({});
    for (let i = 0; i < 30; i++) {
        const random1000 = Math.floor(Math.random() * 1000);
        const random11 = Math.floor(Math.random() * 11);
        const price = Math.floor(Math.random() * 20) + 10;
        const imgs = [
            {
                url: 'https://res.cloudinary.com/dexe271xh/image/upload/v1660411834/Camps/9_dqayf3.jpg',
                filename: 'Camps/9_dqayf3'
            },
            {
                url: 'https://res.cloudinary.com/dexe271xh/image/upload/v1660411834/Camps/8_cteuef.jpg',
                filename: 'Camps/8_cteuef'
            },
            {
                url: 'https://res.cloudinary.com/dexe271xh/image/upload/v1660411834/Camps/11_frgolj.jpg',
                filename: 'Camps/11_frgolj'
            },
            {
                url: 'https://res.cloudinary.com/dexe271xh/image/upload/v1660411834/Camps/7_rncjk9.jpg',
                filename: 'Camps/7_rncjk9'
            },
            {
                url: 'https://res.cloudinary.com/dexe271xh/image/upload/v1660411834/Camps/10_nqcevk.jpg',
                filename: 'Camps/10_nqcevk'
            },
            {
                url: 'https://res.cloudinary.com/dexe271xh/image/upload/v1660411834/Camps/5_lulwdl.jpg',
                filename: 'Camps/5_lulwdl'
            },
            {
                url: 'https://res.cloudinary.com/dexe271xh/image/upload/v1660411834/Camps/3_xxzx4a.jpg',
                filename: 'Camps/3_xxzx4a'
            },
            {
                url: 'https://res.cloudinary.com/dexe271xh/image/upload/v1660411834/Camps/4_n7an3l.jpg',
                filename: 'Camps/4_n7an3l'
            },
            {
                url: 'https://res.cloudinary.com/dexe271xh/image/upload/v1660411834/Camps/6_gndylt.jpg',
                filename: 'Camps/6_gndylt'
            },
            {
                url: 'https://res.cloudinary.com/dexe271xh/image/upload/v1660411834/Camps/2_fzmyju.jpg',
                filename: 'Camps/2_fzmyju'
            },
            {
                url: 'https://res.cloudinary.com/dexe271xh/image/upload/v1660411834/Camps/1_vzvfdz.jpg',
                filename: 'Camps/1_vzvfdz'
            }
        ]
        const camp = new Campground({
            author: '62f7d625b29244688ca0fcd6', //YOUR USER ID
            location: `${cities[random1000].city}, ${cities[random1000].state}`,
            title: `${sample(descriptors)} ${sample(places)}`,
            description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quibusdam dolores vero perferendis laudantium, consequuntur voluptatibus nulla architecto, sit soluta esse iure sed labore ipsam a cum nihil atque molestiae deserunt!',
            price,
            geometry: {
                type: "Point",
                coordinates: [
                    cities[random1000].longitude,
                    cities[random1000].latitude,
                ]
            },
            images: [
                imgs[random11],
                imgs[random11+1]
            ]
        })
        await camp.save();
    }
}

seedDB().then(() => {
    mongoose.connection.close();
})